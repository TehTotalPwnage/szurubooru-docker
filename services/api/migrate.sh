#!/bin/bash

##
# This is an script to upgrade the database using alembic during first installation.
# As in the case with any migrations, make sure to make database backups before continuing!
##
if [ "$(whoami)" != "szurubooru" ]; then
    echo "Rerunning script as API user."
    exec su -c /usr/local/bin/alembic szurubooru
fi

echo "Are you sure you want to continue the migration script? (Y/n)"
read -n1 run

if [ "$run" = "Y" ]; then
    echo "Running migration script."
    echo "Waiting 15 seconds for Postgres to be ready."
    sleep 15

    cd /var/www/html/szurubooru/server
    alembic upgrade head

    echo "Migrations complete."
else
    echo "Exiting."
fi
